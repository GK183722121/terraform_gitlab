# --- root/main.tf --- 

module "vpc" {
  source           = "./vpc"
  vpc_cidr         = "10.0.0.0/16"
  public_sn_count  = 2
  public_cidrs     = ["10.0.1.0/24", "10.0.2.0/24"]
  max_subnet       = 2
  access_ip        = "0.0.0.0/0"
}

module "lam" {
  source           = "./lam"
}