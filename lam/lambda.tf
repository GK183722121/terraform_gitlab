// Create AWS Lambda functions

resource "aws_lambda_function" "lambda-one" {
  filename      = "./lam/lambda.zip"
  function_name = "step-functions-lambda-one"
  role          = aws_iam_role.lambda_assume_role.arn
  handler       = "index.handler"
  runtime       = "python3.8"

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_lambda_layer_version" "data_aggregrater_layer" {
#  filename = "./lam/requestmodulelayer.zip"
  filename = "./lam/lambda.zip"  
  layer_name = "data_aggregrater_layer"
  compatible_runtimes = ["python3.8"]
}





// Lambda IAM assume role
resource "aws_iam_role" "lambda_assume_role" {
  name               = "${var.lambda_function_name}-assume-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_document.json

  lifecycle {
    create_before_destroy = true
  }
}

// IAM policy document for lambda assume role
data "aws_iam_policy_document" "lambda_assume_role_policy_document" {
  version = "2012-10-17"

  statement {
    sid     = "LambdaAssumeRole"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}
